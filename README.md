# Test Tasks

# Task 1
This tasks will look at the methods you use to
optimise code for good in app performance.

#### Part 1:
In order to complete this task you will need to
display a list of 2000 shopping items.
- This list should be randomly generated and be 2000
items long. (Duplicate items are okay, as long as they are random)
- The possible items are provided in the consts.dart file that should be in the current directory.
- Each item in the list should be capitalised

#### Part 2:
In order to complete this part of the task you will
to add some functionality to the app.    
Please add a way to add items to the shopping list.
- The items added should always go to the top of the list.
- You may use libraries but they should not be needed.


# Task 2
This tasks will look at the methods you use to optimise and debug code.

In order to complete this task you will need to debug this simple app. There are errors and code discrepancies through out the 3 files in this app.

You will need to find and fix them in order to complete this task.


# Task 3

This task involves you writing a full application.
The application is a simple dice rolling application.

With this application the user can select the number of dice to roll and roll them.
After which they must be able to reroll any number of dice (without rerolling the entire dice roll)

For example:
- User decides to roll 6 dice.
- The user rolls the dice (the result should be shown on screen)
- The user can then select any number of dice that have already been rolled and reroll those dice
- The result should be updated to show the new values.

For this project you can use only state management libraries (such as BLOC or PROVIDER) and any assets that you wish. (Remember to import them).



#### Notes:
Please comment your code where possible (if time allows for it)
- Libraries are allowed to be used but are not necessary for the task at hand.

